# Win2-7(Pixmap) theme

This is a "fork" of [Win2-7(Pixmap)](https://www.gnome-look.org/content/show.php/?content=118227)-theme.

Goal of this fork is:
 - Fix current issues
 - Support GKT3.x

**Note:** GTK3 support is under development and latest `0.1.0` release just
uses Adwaita theme for GTK3.

## Compatibility

If someone wants to maintain support for other distributions or DE's than those
listed below, feel free to contact / send pull requests.

### Distributions

Only supported distribution is Debian where the supported release
will be always the current stable release.

**Currently supported release(s):**
- Debian 9 (Stretch)

### Desktop Enviroments

Only supported DE is Xfce4.

## Notes

In GTK3 applications the best result from the theme is got when you have
`gtk3-nocsd` package installed and configured.

**Note:** The Debian package of the theme will have the `gtk3-nocsd` package as
a dependency and at least on Debian it is configured automatically without need
to do any manual work, just restart or logout and login after first time
installing the package.

## PPA Repository

Debian package is available at [ppa.lihis.net](http://ppa.lihis.net).
